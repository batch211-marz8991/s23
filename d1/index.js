console.log("Hello World");

//Object - data type to present real world objects

//key - property of an object
//value - data stored in an object


// 2 ways in creating object
//  1. let object = {}
//  2. let object = new object()

//object literal notation (let object = {})

let cellphone = {
    name: "Nokia 3210",
    manufatureDate: 1999
};
console.log("Result from creating object using literal notation:");
console.log(cellphone);

let cellphone2 = {
    name: "Samsung Galaxy",
    manufatureDate: 2015
};
console.log("Result from creating object using literal notation:");
console.log(cellphone2);

let ninja = {
    name: "Naruto Uzumaki",
    village: "Konoha",
    children: ["Boruto","Himawari"]
};
console.log(ninja);



//object constructor notation (let object = new object())

function Laptop(name,manufatureDate){
    this.name = name;
    this.manufatureDate = manufatureDate;
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating object using object constuctor:");
console.log(laptop);
 
let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating object using object constuctor:");
console.log(myLaptop);

let yourLaptop = new Laptop("Asus", 2022);
let ourLaptop = new Laptop("Acer", 2018);
let niceLaptop = new Laptop("HP", 2016);

console.log(yourLaptop);
console.log(ourLaptop);
console.log(niceLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 200);
console.log("Result from creating object using object constuctor:");
console.log(oldLaptop);//undifined

//create empty objects

let computer = {};
let myComputer = new Object();

console.log(computer);
console.log(myComputer);

//accessing Object Properties

//using dot notation
    console.log("Result from dot notation: " + myLaptop.name);

//using square bracket notation
    console.log("result from square bracket notation: " + myLaptop["name"]);

//Accessing array of objects
let arrayObj = [laptop,myLaptop];
console.log(arrayObj[0]["name"]);

console.log(arrayObj[0].name);

//initializing/adding/deleting/reassigning obj property

//using object literal
let car = {};
console.log("Current value of car object: ");
console.log(car);

car.name = "Honda Civic";
console.log("Result from adding object using dot notation");
console.log(car);

//Using bracket notation - not recommendad

console.log();
console.log("Result from adding object using square bracket notation");
car["manufature date"] = 2019;
console.log(car);

//delete object propertie
console.log("Result from deleting properties");
delete car["manufature date"];
console.log(car);

car["manufatureDate"] = 2019;
console.log(car);

//reassigning object property
 car.name = "Toyota Vios";
 console.log("Result from reassigning object property");

 console.log(car);

 //Object methods
 let person = {
    name: "john",
    talk: function(){
        console.log("Hello my name is " + this.name)
    }
 }
 console.log(person);
 console.log("result from object method;")
 person.talk();

 person.walk = function(steps){
    console.log(this.name + " walk " + steps + " steps forward");
 };
 console.log(person);
 person.walk(50);


 let friend = {
    firstName: "Joe",
    lastName: "Smith",
    adress: {
        city: "Austin",
        country: "Texas"
    },
    emails: ["joe@gmail.com","joesmith@mail.xyz"],
    introduce: function(){
        console.log("Hello my name is " + this.firstName + ", " + this.lastName + ", " + "I lived in " + this.adress.city + ", " + this.adress.country)
    }
 }
friend.introduce();

//real world application of objects

function Pokemon(name,level){
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attact = level;
    this.tackle = function(target){
        console.log(this.name + "tackled " + target.name);


        target.health -= this.attact

        console.log(target.name + " health is now reduce " + target.health)

        if(target.health<=0){
            target.faint()
        }
    }
    this.faint = function(){
        console.log(this.name + " fainted.")

    }
}
let pikachu = new Pokemon ("Pikachu", 88);
console.log(pikachu);

let ratata = new Pokemon ("Ratata", 10);
console.log(ratata)